package com.rigem.api.books.response;

public class CategoriaResponseRest extends ResponseRest{
	private CategoriaResponse categoriaresponse;

	public CategoriaResponse getCategoriaresponse() {
		return categoriaresponse;
	}

	public void setCategoriaresponse(CategoriaResponse categoriaresponse) {
		this.categoriaresponse = categoriaresponse;
	}
	
	
}
