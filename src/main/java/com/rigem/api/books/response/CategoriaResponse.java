package com.rigem.api.books.response;

import java.util.List;

import com.rigem.api.books.models.Categoria;

public class CategoriaResponse {

	private List<Categoria> categoria;

	public List<Categoria> getCategoria() {
		return categoria;
	}

	public void setCategoria(List<Categoria> categoria) {
		this.categoria = categoria;
	}
	
	
}
