package com.rigem.api.books.response;

import java.util.ArrayList;
import java.util.HashMap;

public class ResponseRest {
	private ArrayList<HashMap<String, String>> metadata = new ArrayList<>();
	
	public ArrayList<HashMap<String,String>> getMetadata(){
		return metadata;
	}
	
	public void setMetadata(String codigo,String tipo,String date) {
		HashMap<String, String> mapa = new HashMap<String,String>();
		mapa.put("codigo", codigo);
		mapa.put("tipo", tipo);
		mapa.put("date", date);
		metadata.add(mapa);
	}
}
