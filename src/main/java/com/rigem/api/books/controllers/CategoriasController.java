package com.rigem.api.books.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rigem.api.books.response.CategoriaResponse;
import com.rigem.api.books.response.CategoriaResponseRest;
import com.rigem.api.books.services.ICategoriaService;

@RestController
@RequestMapping("/v1")
public class CategoriasController {

	@Autowired
	public ICategoriaService service;
	
	@GetMapping("/categorias")
	public CategoriaResponseRest consultarCat() {
		CategoriaResponseRest response = service.buscarCategorias();
		return response;
	} 
}
