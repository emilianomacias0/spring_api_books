package com.rigem.api.books.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rigem.api.books.models.Categoria;
import com.rigem.api.books.models.dao.ICategoriaDao;
import com.rigem.api.books.response.CategoriaResponseRest;

@Service
public class CategoriaServicesImpl implements ICategoriaService{
	
	private static final Logger log = LoggerFactory.getLogger(CategoriaServicesImpl.class);
	
	@Autowired
	private ICategoriaDao categoriaDao;

	@Override
	@Transactional(readOnly = true)
	public CategoriaResponseRest buscarCategorias() {

		log.info("Inicio del metodo buscarCategorias()");
		
		CategoriaResponseRest response = new CategoriaResponseRest();
		
		try {
			List<Categoria> categoria = (List<Categoria>) categoriaDao.findAll();
			response.getCategoriaresponse().setCategoria(categoria);
			
			response.setMetadata("00","Respuesta ok", "Respuesta exitosa");
		}catch(Exception e) {
			response.setMetadata("-1","Respuesta not ok", "Respuesta incorrecta");
			log.error("Error al consultar categoria",e.getMessage());
			e.getStackTrace();
		}
		
		return response;
	}

}
