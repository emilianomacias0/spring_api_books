package com.rigem.api.books.services;

import com.rigem.api.books.response.CategoriaResponseRest;

public interface ICategoriaService {

	public CategoriaResponseRest buscarCategorias();
	
}
