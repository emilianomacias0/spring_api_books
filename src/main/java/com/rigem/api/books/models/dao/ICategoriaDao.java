package com.rigem.api.books.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.rigem.api.books.models.Categoria;

public interface ICategoriaDao extends CrudRepository<Categoria, Long> {

	
}
